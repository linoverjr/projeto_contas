package br.com.contas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.contas.entity.Contas;
import br.com.contas.service.ContasService;



@RestController
@RequestMapping("/api/v1")
public class ContasController {

	@Autowired
	private ContasService contasService;
	
	@PostMapping("/contas/")
	public Contas save(@RequestBody Contas conta) {
		//condição de existência
		return contasService.saveConta(conta);
	}
	
	@PutMapping("/contas/{id}/")
	public Contas update(@RequestBody Contas conta) {
		return contasService.updateConta(conta);
	}
	
	@GetMapping("/contas/")
	public List<Contas> getAllContas() {
		return contasService.getAllContas();
	}
	
	@GetMapping("/contas/{id}/")
	public Contas getConta(@PathVariable (name = "id") Integer id) {
		return contasService.getConta(id);
	}

	
	@DeleteMapping("/contas/{id}/")
	public void deleteConta(@PathVariable (name = "id") Integer id) {
		contasService.deleteConta(id);
	}
	
}
