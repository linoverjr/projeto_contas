package br.com.contas.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.contas.dao.ContasDao;
import br.com.contas.entity.Contas;
import br.com.contas.service.ContasService;

@Service
public class ContasImplement implements ContasService{

	
	@Autowired
	private ContasDao contasDao;
	
	@Override
	public Contas saveConta(Contas conta) {
		return contasDao.save(conta);
	}

	@Override
	public Contas updateConta(Contas conta) {
		return contasDao.saveAndFlush(conta);
	}

	@Override
	public List<Contas> getAllContas() {
		return contasDao.findAll();
	}

	@Override
	public Contas getConta(Integer contaId) {
		return contasDao.getOne(contaId);
	}

	@Override
	public void deleteConta(Integer contaId) {
		contasDao.deleteById(contaId);
		
	}

}
