package br.com.contas.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.contas.entity.Contas;

@Repository
public interface ContasDao extends JpaRepository<Contas, Integer> {

}