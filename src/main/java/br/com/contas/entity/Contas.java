package br.com.contas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "contas")
@Setter
@Getter
public class Contas {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotBlank
	private Integer contaId;
	
	@Column(name = "num_agencia")
	@NotBlank
	private Integer agencia;
 
	@Column(name = "num_conta")
	@NotBlank
	private Integer conta;
    
	@Column(name = "cod_banco")
	@NotBlank
	private Integer codigoBanco;
	
}