package br.com.contas.service;

import java.util.List;

import br.com.contas.entity.Contas;

public interface ContasService {
	
	Contas saveConta(Contas conta);
	
	Contas updateConta(Contas conta);
	
	List<Contas> getAllContas();
	
	Contas getConta(Integer contaId);
	
	void deleteConta(Integer contaId);

}